﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Google_Map_API.Startup))]
namespace Google_Map_API
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
